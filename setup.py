'''setuptools script for clang based pygments lexer package
'''

import os.path

from setuptools import setup
from setuptools import find_packages

setup(
    name="pygclang",
    version="0.2",
    description='A clang based lexer for the pygments highlighting package.',
    url="https://bitbucket.org/fun4jimmy/pygclang",
    author="James Whitworth",
    license='MIT',
    packages=find_packages(),
    package_data={
        'pygclang': [
            os.path.join('libclang-3.5', 'windows', 'libclang.dll')
        ],
    },
    zip_safe=False, # we need to be able to get to the libclang binary
    entry_points={'pygments.lexers': ['ClangLexer = pygclang:ClangLexer']},
    install_requires=[
        'clang==3.5',
        'pygments',
    ],
    test_suite="tests",
)
