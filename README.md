# README #

This is a [clang](http://clang.llvm.org/) based lexer plugin for the [pygments](http://pygments.org/) syntax highlighter.

### What is this repository for? ###

* Using libclang to parse C/C++ code provides a more thorough highlight than the default [pygments regex based highlighter](http://pygments.org/docs/lexers/#lexers-for-c-c-languages).
* Providing the lexer as a [setup.py](https://docs.python.org/2/distutils/setupscript.html) package with pygments [entry points](https://pythonhosted.org/setuptools/setuptools.html#extensible-applications-and-frameworks) allows for use with other frameworks that use pygments such as the [ruby pygments gem](https://github.com/tmm1/pygments.rb) or [Jekyll](http://jekyllrb.com/).

### How do I get set up? ###

* Clone this repository.
```
#!bash

$ git clone https://bitbucket.org/fun4jimmy/pygclang.git
```

* Install the package. This will also install any required dependencies including pygments if you don't already have it.
```
#!bash

$ cd pygclang
$ python setup.py install
```

* Highlight some code.
```
#!bash

$ pygmentize -f html -l clang -o output.html <path_to_some_c_file>
```
or in python

```
#!python

from pygments import highlight
from pygments.formatters import HtmlFormatter

from pygclang import ClangLexer

source = '''#include <iostream>

int main(int, char**)
{
  std::cout << "Hello World!" << std::endl;
  return 0;
}
'''

print highlight(source, ClangLexer(), HtmlFormatter())
```