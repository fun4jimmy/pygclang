'''
'''
import clang.cindex
import fnmatch
import logging
import os.path
import platform
import pygments.lexer
import pygments.token

# disable line to long pylint warning
#pylint: disable=I0011,C0301

def _get_clang_library_file():
    '''Finds the libclang dll or shared library.
    On linux we search /usr/lib for libclang-3.5.so*
    For windows the dll is bundled for simplicity.
    '''

    platform_system = platform.system().lower()
    if platform_system == 'windows':
        this_directory = os.path.dirname(__file__)
        this_directory = os.path.abspath(this_directory)

        libclang_file = os.path.join(this_directory, 'libclang-3.5', platform_system, 'libclang.dll')
        libclang_file = os.path.normpath(libclang_file)
    elif platform_system == 'linux':
        # search for libclang in /usr/lib
        for root, dirnames, filenames in os.walk('/usr/lib'):
            # this allows matching libclang-3.5.so.1 as well as libclang-3.5.so
            for filename in fnmatch.filter(filenames, 'libclang-3.5.so*'):
                libclang_file = os.path.join(root, filename)

        if not libclang_file:
            raise RuntimeError("Unable to locate libclang-3.5.so in '/usr/lib' are you sure you have " \
                               "libclang installed? Try 'sudo apt-get install libclang-3.5-dev'.")

    return libclang_file

_CLANG_LIBRARY_FILE = _get_clang_library_file()
clang.cindex.Config.set_library_file(_CLANG_LIBRARY_FILE)

def _is_preprocessor_directive(clang_token, previous_clang_tokens):
    '''Is clang_token the 'include' portion of a '#include' statement.
    '''

    preprocessor_keywords = [
        'comment',
        'define',
        'defined',
        'dllexport',
        'dllimport',
        'elif',
        'else',
        'endif',
        'error',
        'if',
        'ifdef',
        'ifndef',
        'include',
        'pragma',
    ]

    if clang_token.spelling not in preprocessor_keywords:
        return False

    if previous_clang_tokens[-1] != None:
        if previous_clang_tokens[-1].spelling == '#':
            return previous_clang_tokens[-1].kind.name == 'PUNCTUATION'

    if previous_clang_tokens[-2] != None:
        # matches: #if defined
        if previous_clang_tokens[-2].spelling == '#':
            return previous_clang_tokens[-2].kind.name == 'PUNCTUATION'
        # matches: __declspec(dllimport)
        # matches: __declspec(dllexport)
        elif previous_clang_tokens[-2].spelling == '__declspec':
            return True

    return False

def _is_typename(clang_token, previous_clang_tokens):
    '''Does clang_token refer to the name of a type.
    '''

    typename_cursor_kinds = [
        'CLASS_DECL',
        'CLASS_TEMPLATE',
        'CONSTRUCTOR',
        'DESTRUCTOR',
        'TEMPLATE_REF',
        'TEMPLATE_TYPE_PARAMETER',
        'TYPE_REF',
    ]

    if clang_token.cursor.kind.name in typename_cursor_kinds:
        return True
    # for template<size_t N> both size_t and N are returned as IDENTIFIER and TEMPLATE_NON_TYPE_PARAMETER
    # but only the type (size_t) will be preceded by a punctuation token (',' or '<').
    elif clang_token.cursor.kind.name == 'TEMPLATE_NON_TYPE_PARAMETER' and \
         previous_clang_tokens[-1].kind.name == 'PUNCTUATION':
        return True
    # Looking for Typename:
    # friend class Typename;
    # friend struct Typename;
    # friend Typename Function();
    elif clang_token.cursor.kind.name == 'UNEXPOSED_DECL':
        if previous_clang_tokens[-1].kind.name == 'KEYWORD' and \
           previous_clang_tokens[-1].spelling in ['class', 'struct', 'friend', 'typename', 'const', 'volatile']:
            return True
        # if the punctuation is * or & then it is a pointer or reference parameter
        elif previous_clang_tokens[-1].kind.name == 'PUNCTUATION' and \
             previous_clang_tokens[-1].spelling not in ['*', '&']:
            return True
    # template<typename ClassType>
    # template<typename FunctionType>
    # bool Class<ClassType>::Function(FunctionType );
    # the first occurrance of ClassType has a cursor kind of INVALID_FILE
    elif clang_token.cursor.kind.name == 'INVALID_FILE':
        if previous_clang_tokens[-1].kind.name == 'KEYWORD' and \
           previous_clang_tokens[-1].spelling in ['class', 'struct', 'typename']:
            return True

    return False

def _is_variable(clang_token, previous_clang_tokens):
    '''Does clang_token refer to a variable.
    '''

    variable_cursor_kinds = [
        'COMPOUND_STMT', # member variable reference in method ie. class A { int x; int Function() { return x; } };
        'DECL_REF_EXPR', # function parameter reference
        'FIELD_DECL', # member variable declaration
        'MEMBER_REF', # member variable reference in constructor ie. class A { int x; A() : x(0) {} };
        'MEMBER_REF_EXPR', # member variable reference inside an expression
        'PARM_DECL', # function argument
        'TEMPLATE_NON_TYPE_PARAMETER', # N in template<size_t N> 
        'VAR_DECL', # local/global variable
    ]

    if clang_token.cursor.kind.name in variable_cursor_kinds:
        return True
    elif clang_token.cursor.kind.name == 'UNEXPOSED_DECL':
        if previous_clang_tokens[-2].kind.name == 'PUNCTUATION':
            return True
        elif previous_clang_tokens[-1].kind.name == 'PUNCTUATION' and \
             previous_clang_tokens[-1].spelling in ['*', '&']:
            return True

    return False

def _is_function(clang_token, previous_clang_tokens):
    '''Does clang_token refer to a function.
    '''

    function_cursor_kinds = [
        'FUNCTION_DECL', # function declaration
        'CXX_METHOD', 
    ]

    if clang_token.cursor.kind.name in function_cursor_kinds:
        return True
    elif clang_token.cursor.kind.name == 'UNEXPOSED_DECL':
        if previous_clang_tokens[-2].kind.name == 'KEYWORD' and \
           previous_clang_tokens[-2].spelling in ['class', 'struct', 'friend']:
            return True

    return False

def _get_indentifier_token_type(clang_token, previous_clang_tokens):
    '''Returns the token type of a clang token of kind IDENTIFIER.
    '''

    if _is_preprocessor_directive(clang_token, previous_clang_tokens):
        return pygments.token.Token.Comment.Preproc
    if clang_token.cursor.kind.name == 'INCLUSION_DIRECTIVE':
        return pygments.token.Token.String
    if clang_token.cursor.kind.name == 'NAMESPACE':
        return pygments.token.Token.Name.Namespace
    elif _is_typename(clang_token, previous_clang_tokens):
        return pygments.token.Token.Name.Class
    elif _is_variable(clang_token, previous_clang_tokens):
        return pygments.token.Token.Name.Variable
    elif _is_function(clang_token, previous_clang_tokens):
        return pygments.token.Token.Name.Function
    elif clang_token.cursor.kind.name in ['MACRO_DEFINITION', 'MACRO_INSTANTIATION']:
        return pygments.token.Token.Name.Constant

    return pygments.token.Token.Text

def _get_literal_token_type(clang_token):
    '''Returns the token type of a clang token of kind LITERAL.
    '''

    if clang_token.cursor.kind.name == 'INCLUSION_DIRECTIVE':
        return pygments.token.Token.String
    elif clang_token.cursor.kind.name == 'INTEGER_LITERAL':
        return pygments.token.Token.Number.Integer
    elif clang_token.cursor.kind.name == 'FLOATING_LITERAL':
        return pygments.token.Token.Number.Float
    elif clang_token.cursor.kind.name == 'UNEXPOSED_DECL':
        # extern "C"
        if clang_token.spelling == '"C"':
            return pygments.token.Token.String
    elif clang_token.cursor.kind.name == 'INVALID_FILE':
        if clang_token.spelling.startswith('"') and clang_token.spelling.endswith('"'):
            return pygments.token.Token.String

    return pygments.token.Token.Literal

def _get_punctuation_token_type(clang_token):
    '''Returns the token type of a clang token of kind PUNCTUATION.
    '''

    if clang_token.spelling == '#':
        if clang_token.cursor.kind.name in ['INVALID_FILE', 'INCLUSION_DIRECTIVE']:
            return pygments.token.Token.Comment.Preproc    
    elif clang_token.spelling in ['<', '>', '.']:
        if clang_token.cursor.kind.name == 'INCLUSION_DIRECTIVE':
            # triangle brackets include
            return pygments.token.Token.String

    return pygments.token.Token.Punctuation

def _get_token_type(clang_token, previous_clang_tokens):
    '''Convert from a clang token to a pygments token type.
    '''

    token_type = pygments.token.Token.Text

    if clang_token.kind.name == 'COMMENT':
        token_type = pygments.token.Token.Comment
    elif clang_token.kind.name == 'IDENTIFIER':
        token_type = _get_indentifier_token_type(clang_token, previous_clang_tokens)
    elif clang_token.kind.name == 'KEYWORD':
        if _is_preprocessor_directive(clang_token, previous_clang_tokens):
            token_type = pygments.token.Token.Comment.Preproc
        else:
            token_type = pygments.token.Token.Keyword
    elif clang_token.kind.name == 'LITERAL':
        token_type = _get_literal_token_type(clang_token)
    elif clang_token.kind.name == 'PUNCTUATION':
        token_type = _get_punctuation_token_type(clang_token)

    return token_type


class ClangLexer(pygments.lexer.Lexer):
    '''For c/c++ code but using libclang for better highlighting.
    '''

    name = 'clang'
    aliases = ['clang']
    filenames = ['*.c', '*.cpp']

    clang_parse_flags = clang.cindex.TranslationUnit.PARSE_DETAILED_PROCESSING_RECORD | \
                        clang.cindex.TranslationUnit.PARSE_INCOMPLETE | \
                        clang.cindex.TranslationUnit.PARSE_INCLUDE_BRIEF_COMMENTS_IN_CODE_COMPLETION
    clang_parse_arguments = ['-c']

    def get_tokens_unprocessed(self, text):
        '''pygments.lexer overload.'''

        previous_tokens = [None, None]

        # if any call raises an exception the whole text block will be returned as a text token.
        try:
            source_name = 'a.cpp'
            translation_unit = clang.cindex.TranslationUnit.from_source(source_name,
                                                                        ClangLexer.clang_parse_arguments,
                                                                        [(source_name, text)],
                                                                        ClangLexer.clang_parse_flags,
                                                                        None)

            for token in translation_unit.cursor.get_tokens():
                logging.debug('%s %s %s', token.kind, token.cursor.kind, token.spelling)

                # the gap between clang tokens should be returned as a pygments Text token otherwise it's removed
                if previous_tokens[-1]:
                    text_token_index = previous_tokens[-1].location.offset + len(previous_tokens[-1].spelling)
                    text_token_length = token.location.offset - text_token_index
                    text_token_value = text[text_token_index:text_token_index + text_token_length]
                    if text_token_length > 0:
                        yield text_token_index, pygments.token.Token.Text, text_token_value

                index = token.location.offset
                tokentype = _get_token_type(token, previous_tokens)
                # convert to unicode as some formatters try to call translate which is not a member of clang's CXString
                value = unicode(token.spelling)

                logging.debug('%s %s', tokentype, value)

                yield index, tokentype, value

                previous_tokens[-2] = previous_tokens[-1]
                previous_tokens[-1] = token

        finally:
            if previous_tokens[-1]:
                final_token_index = previous_tokens[-1].location.offset + len(previous_tokens[-1].spelling)
            else:
                final_token_index = 0

            final_token_value = text[final_token_index:]
            yield final_token_index, pygments.token.Token.Text, final_token_value
