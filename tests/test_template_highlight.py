'''Unit tests for module pygclang.'''

# disable warning about too many public methods and disable warning about locally disabling that warning
#pylint: disable=I0011,R0904

# disable warning about local import and disable warning about locally disabling that warning
#pylint: disable=I0011,W0403

from pygclang_fixture import ClangLexerTestFixture

class TestClangLexer(ClangLexerTestFixture):
    '''Test case for ClangLexer'''

    def test_template_typename(self):
        '''Test the syntax highlighting of a templated class forward declaration.'''

        text = 'template<typename T> class ForwardDecl;'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_text, 'template')
        self.assertHighlightedAsPunctuation(highlighted_text, '<')
        self.assertHighlightedAsKeyword(highlighted_text, 'typename')
        self.assertHighlightedAsClassName(highlighted_text, 'T')
        self.assertHighlightedAsPunctuation(highlighted_text, '>')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_template_non_typename(self):
        '''Test the syntax highlighting of a templated class forward declaration.'''

        text = 'template<size_t N> class ForwardDecl;'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_text, 'template')
        self.assertHighlightedAsPunctuation(highlighted_text, '<')
        self.assertHighlightedAsClassName(highlighted_text, 'size_t')
        self.assertHighlightedAsVariableName(highlighted_text, 'N')
        self.assertHighlightedAsPunctuation(highlighted_text, '>')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_template_method_definition(self):
        '''Test the syntax highlighting of a templated class method defined outside the class declaration.'''

        text = '''template<typename T>
class Base { void function(); };
template<typename T> void Base<T>::function() {}'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsClassName(highlighted_lines[2], 'Base')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'function')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_template_method_definition_with_function_template(self):
        '''Test the syntax highlighting of a templated class method defined outside the class declaration.'''

        text = '''template<typename T>
class Base { template<typename U> void function(U a); };
template<typename T>
template<typename U>
void Base<T>::function(U a) {}'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsClassName(highlighted_lines[2], 'T')
        self.assertHighlightedAsClassName(highlighted_lines[3], 'U')

        self.assertHighlightedAsClassName(highlighted_lines[4], 'T')
        self.assertHighlightedAsClassName(highlighted_lines[4], 'U')
        self.assertHighlightedAsVariableName(highlighted_lines[4], 'a')
        self.assertNothingWasStripped(text, highlighted_lines)

if __name__ == '__main__':
    import unittest
    unittest.main()
