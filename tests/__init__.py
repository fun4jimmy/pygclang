'''__init__.py file for tests package.'''

import logging
import os.path
import unittest

def main():
    '''Discovers all tests in the package directory, called on package import.'''

    logging.basicConfig(level=logging.INFO)

    this_directory = os.path.normpath(os.path.abspath(os.path.dirname(__file__)))
    suite = unittest.defaultTestLoader.discover(this_directory, pattern='test_*.py')
    unittest.TextTestRunner(verbosity=2).run(suite)

if __name__ == '__main__':
    main()
