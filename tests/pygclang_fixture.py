'''Unit tests for module pygclang.'''

# disable warning about assertNothingWasStripped function name and disable warning about locally disabling that warning
#pylint: disable=I0011,C0103

# disable warning about too many public methods and disable warning about locally disabling that warning
#pylint: disable=I0011,R0904

import os.path
import re
import sys
import unittest

from pygments import highlight
from pygments.formatters import HtmlFormatter
from xml.sax.saxutils import escape
from xml.sax.saxutils import unescape

_TESTS_DIRECTORY = os.path.dirname(__file__)
_ROOT_DIRECTORY = os.path.normpath(os.path.abspath(os.path.join(_TESTS_DIRECTORY, '..')))
sys.path.append(_ROOT_DIRECTORY)

import pygclang

_HTML_ESCAPE_TABLE = {
    '"': "&quot;",
    "'": "&apos;"
}
_HTML_UNESCAPE_TABLE = {
    v:k for k, v in _HTML_ESCAPE_TABLE.items()
}
_HTML_TAG_REGEX = re.compile(r'<.*?>')

def _escape_for_html(text):
    '''Escapes a string of text for use in an html block.'''
    return escape(text, _HTML_ESCAPE_TABLE)

def _html_to_raw(html_text):
    '''Strips all html tags from a block of html returning the raw text.'''
    text = _HTML_TAG_REGEX.sub('', html_text)
    text = unescape(text, _HTML_UNESCAPE_TABLE)
    # the highlighting appends two newline characters that we should also remove
    return text.rstrip()

class ClangLexerTestFixture(unittest.TestCase):
    '''Test fixture for ClangLexer'''

    @classmethod
    def highlight(cls, text):
        '''Invokes pygments with ClangLexer to highlight a block of text.'''
        return highlight(text, pygclang.ClangLexer(), HtmlFormatter())


    def assertNothingWasStripped(self, text, highlighted_text):
        '''Removes all markup and unescapes the highlighted output and checks if it matches the original.'''

        if isinstance(highlighted_text, list):
            highlighted_text = '\n'.join(highlighted_text)

        self.assertEqual(text, _html_to_raw(highlighted_text))

    def assertTextHasSpanWithClass(self, highlighted_text, text, class_type):
        '''Checks a class is wrapped in a span tag with the given class type.'''

        escaped_text = _escape_for_html(text)
        self.assertIn('<span class="{}">{}</span>'.format(class_type, escaped_text), highlighted_text)

    def assertHighlightedAsPreprocessor(self, highlighted_text, text):
        '''Checks text was highlighted as a preprocessor directive.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'cp')

    def assertHighlightedAsPunctuation(self, highlighted_text, text):
        '''Checks text was highlighted as punctuation.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'p')

    def assertHighlightedAsString(self, highlighted_text, text):
        '''Checks text was highlighted as a string.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 's')

    def assertHighlightedAsInteger(self, highlighted_text, text):
        '''Checks text was highlighted as an integer.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'mi')

    def assertHighlightedAsFloat(self, highlighted_text, text):
        '''Checks text was highlighted as a float.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'mf')

    def assertHighlightedAsKeyword(self, highlighted_text, text):
        '''Checks text was highlighted as a keyword.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'k')

    def assertHighlightedAsConstantName(self, highlighted_text, text):
        '''Checks text was highlighted as a constant name.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'no')

    def assertHighlightedAsClassName(self, highlighted_text, text):
        '''Checks text was highlighted as a class name.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'nc')

    def assertHighlightedAsNamespaceName(self, highlighted_text, text):
        '''Checks text was highlighted as a namespace name.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'nn')

    def assertHighlightedAsVariableName(self, highlighted_text, text):
        '''Checks text was highlighted as a variable name.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'nv')

    def assertHighlightedAsFunctionName(self, highlighted_text, text):
        '''Checks text was highlighted as a function name.'''

        self.assertTextHasSpanWithClass(highlighted_text, text, 'nf')
