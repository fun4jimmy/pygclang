'''Unit tests for module pygclang.'''

# disable warning about too many public methods and disable warning about locally disabling that warning
#pylint: disable=I0011,R0904

# disable warning about local import and disable warning about locally disabling that warning
#pylint: disable=I0011,W0403

from pygclang_fixture import ClangLexerTestFixture

class TestFriendHighlight(ClangLexerTestFixture):
    '''Test case for ClangLexer highlighting of friend declarations.'''

    def test_friend_class(self):
        '''Test the syntax highlighting of a friend class.'''

        text = '''class Base
{
  friend class Other;
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'class')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'Other')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ';')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_struct(self):
        '''Test the syntax highlighting of a friend struct.'''

        text = '''class Base
{
  friend struct Other;
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'struct')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'Other')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ';')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_function_return_void(self):
        '''Test the syntax highlighting of a friend function with no arguments returning void.'''

        text = '''class Base
{
  friend void Function();
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'void')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'Function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '();')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_function_return_void_unnamed_type(self):
        '''Test the syntax highlighting of a friend function with an unnamed type argument returning void.'''

        text = '''class Base
{
  friend void Function(int32_t);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'void')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'Function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '(')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'int32_t')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ');')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_function_return_void_named_type(self):
        '''Test the syntax highlighting of a friend function with a named type argument returning void.'''

        text = '''class Base
{
  friend void Function(int32_t param);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'void')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'Function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '(')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'int32_t')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'param')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ');')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_function_return_type(self):
        '''Test the syntax highlighting of a friend function with no arguments returning a type.'''

        text = '''class Base
{
  friend int32_t Function();
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'int32_t')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'Function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '();')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_function_return_type_unnamed_type(self):
        '''Test the syntax highlighting of a friend function with an unnamed type argument returning a type.'''

        text = '''class Base
{
  friend int32_t Function(Type);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'int32_t')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'Function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '(')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'Type')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ');')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_function_return_type_named_type(self):
        '''Test the syntax highlighting of a friend function with a named type argument returning a type.'''

        text = '''class Base
{
  friend int32_t Function(Type param);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'int32_t')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'Function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '(')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'Type')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'param')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ');')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_function_multiple_named_types(self):
        '''Test the syntax highlighting of a friend function with multiple named type arguments.'''

        text = '''class Base
{
  friend void Function(Type a, OtherType b);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'Function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '(')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'Type')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'a')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'OtherType')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'b')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ');')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_template_friend_class(self):
        '''Test the syntax highlighting of a templated friend class.'''

        text = '''class A
{
  template<typename T>
  friend class B;
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'template')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '<')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'typename')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'T')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '>')

        self.assertHighlightedAsKeyword(highlighted_lines[3], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[3], 'class')
        self.assertHighlightedAsClassName(highlighted_lines[3], 'B')
        self.assertHighlightedAsPunctuation(highlighted_lines[3], ';')

        self.assertNothingWasStripped(text, highlighted_lines)

    def test_non_typename_template_friend_class(self):
        '''Test the syntax highlighting of a non typename templated friend class.'''

        text = '''class A
{
  template<size_t N>
  friend class B;
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'template')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '<')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'size_t')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'N')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '>')

        self.assertHighlightedAsKeyword(highlighted_lines[3], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[3], 'class')
        self.assertHighlightedAsClassName(highlighted_lines[3], 'B')
        self.assertHighlightedAsPunctuation(highlighted_lines[3], ';')

        self.assertNothingWasStripped(text, highlighted_lines)

    def test_template_friend_function(self):
        '''Test the syntax highlighting of a templated friend function.'''

        text = '''class A
{
  template<typename T>
  friend void Function(T blah);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'template')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '<')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'typename')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'T')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '>')

        self.assertHighlightedAsKeyword(highlighted_lines[3], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[3], 'void')
        self.assertHighlightedAsFunctionName(highlighted_lines[3], 'Function')
        self.assertHighlightedAsPunctuation(highlighted_lines[3], '(')
        self.assertHighlightedAsClassName(highlighted_lines[3], 'T')
        self.assertHighlightedAsVariableName(highlighted_lines[3], 'blah')
        self.assertHighlightedAsPunctuation(highlighted_lines[3], ');')

        self.assertNothingWasStripped(text, highlighted_lines)

    def test_friend_function_pointer_type(self):
        '''Test the syntax highlighting of a friend function taking a pointer argument'''

        text = '''class A
{
  friend void function(void *param);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'void')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '(')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '*')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'param')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ');')

    def test_friend_function_qualified_type(self):
        '''Test the syntax highlighting of a friend function taking a pointer argument'''

        text = '''class A
{
  friend void function(const int32_t *param);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'friend')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'void')
        self.assertHighlightedAsFunctionName(highlighted_lines[2], 'function')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '(')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'const')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'int32_t')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '*')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'param')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ');')

if __name__ == '__main__':
    import unittest
    unittest.main()
