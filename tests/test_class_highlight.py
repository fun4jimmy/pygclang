'''Unit tests for module pygclang.'''

# disable warning about too many public methods and disable warning about locally disabling that warning
#pylint: disable=I0011,R0904

# disable warning about local import and disable warning about locally disabling that warning
#pylint: disable=I0011,W0403

from pygclang_fixture import ClangLexerTestFixture

class TestClassHighlight(ClangLexerTestFixture):
    '''Test case for ClangLexer highlighting of classes.'''

    def test_class_forward_declaration(self):
        '''Test the syntax highlighting of a class forward declaration.'''

        text = 'class ForwardDecl;'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_text, 'class')
        self.assertHighlightedAsClassName(highlighted_text, 'ForwardDecl')
        self.assertHighlightedAsPunctuation(highlighted_text, ';')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_constructor(self):
        '''Test the syntax highlighting of a class constructor.'''

        text = '''class Base
{
  Base();
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsClassName(highlighted_lines[2], 'Base')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '();')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_constructor_argument(self):
        '''Test the syntax highlighting of a class constructor with an argument.'''

        text = '''class Base
{
  Base(int argument);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsClassName(highlighted_lines[2], 'Base')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '(')
        self.assertHighlightedAsKeyword(highlighted_lines[2], 'int')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'argument')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ');')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_constructor_argument_user_type(self):
        '''Test the syntax highlighting of a class constructor with a user defined type argument.'''

        text = '''class Other;
class Base
{
  Base(Other argument);
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsClassName(highlighted_lines[3], 'Other')
        self.assertHighlightedAsVariableName(highlighted_lines[3], 'argument')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_destructor(self):
        '''Test the syntax highlighting of a class destructor.'''

        text = '''class Base
{
  ~Base();
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsPunctuation(highlighted_lines[2], '~')
        self.assertHighlightedAsClassName(highlighted_lines[2], 'Base')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], '();')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_member(self):
        '''Test the syntax highlighting of a member variable.'''

        text = '''class Base
{
  int m_member;
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[2], 'int')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'm_member')
        self.assertHighlightedAsPunctuation(highlighted_lines[2], ';')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_member_reference(self):
        '''Test the syntax highlighting of a member variable reference.'''

        text = '''class Base
{
  int m_member;

  void method()
  {
    m_member;
  }
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsVariableName(highlighted_lines[6], 'm_member')
        self.assertHighlightedAsPunctuation(highlighted_lines[6], ';')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_member(self):
        '''Test the syntax highlighting of a member variable of a user defined type.'''

        text = '''class Other;
class Base
{
  Other m_member;
};'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsClassName(highlighted_lines[3], 'Other')
        self.assertHighlightedAsVariableName(highlighted_lines[3], 'm_member')
        self.assertNothingWasStripped(text, highlighted_lines)

if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)
    ClangLexerTestFixture.highlight('''class Base
{
  Base(Other parameter);
  Other *m_member;
};''')
    logging.getLogger().setLevel(logging.INFO)

    import unittest
    unittest.main()
