'''Unit tests for module pygclang.'''

# disable warning about too many public methods and disable warning about locally disabling that warning
#pylint: disable=I0011,R0904

# disable warning about local import and disable warning about locally disabling that warning
#pylint: disable=I0011,W0403

from pygclang_fixture import ClangLexerTestFixture

class TestCppHighlight(ClangLexerTestFixture):
    '''Test case for ClangLexer highlighting of c++ specific concepts.'''

    def test_namespace(self):
        '''Test the syntax highlighting of a namespace.'''

        text = '''namespace Namespace
{
}'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[0], 'namespace')
        self.assertHighlightedAsNamespaceName(highlighted_lines[0], 'Namespace')
        self.assertNothingWasStripped(text, highlighted_lines)

if __name__ == '__main__':
    import unittest
    unittest.main()
