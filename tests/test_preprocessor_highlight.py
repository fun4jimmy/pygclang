'''Unit tests for module pygclang.'''

# disable warning about too many public methods and disable warning about locally disabling that warning
#pylint: disable=I0011,R0904

# disable warning about local import and disable warning about locally disabling that warning
#pylint: disable=I0011,W0403

from pygclang_fixture import ClangLexerTestFixture

class TestPreprocessorHighlight(ClangLexerTestFixture):
    '''Test case for ClangLexer highlighting of preprocessor directives'''

    def test_quote_include(self):
        '''Test the syntax highlighting of a quoted include'''

        text = '#include "hello.h"'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsPreprocessor(highlighted_text, '#include')
        self.assertHighlightedAsString(highlighted_text, '"hello.h"')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_bracket_include(self):
        '''Test the syntax highlighting of a bracketed include'''

        text = '#include <hello.h>'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsPreprocessor(highlighted_text, '#include')
        self.assertHighlightedAsString(highlighted_text, '<hello.h>')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_define(self):
        '''Test the syntax highlighting of a bracketed include'''

        text = '#define DEFINE'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsPreprocessor(highlighted_text, '#define')
        self.assertHighlightedAsConstantName(highlighted_text, 'DEFINE')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_if(self):
        '''Test the syntax highlighting of a bracketed include'''

        text = '''#if 0
#endif'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsPreprocessor(highlighted_lines[0], '#if')
        self.assertHighlightedAsPreprocessor(highlighted_lines[1], '#endif')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_if_defined(self):
        '''Test the syntax highlighting of a bracketed include'''

        text = '''#if defined(CONDITION)
#endif'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsPreprocessor(highlighted_lines[0], '#if')
        self.assertHighlightedAsPreprocessor(highlighted_lines[0], 'defined')
        self.assertHighlightedAsPreprocessor(highlighted_lines[1], '#endif')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_defined_macro(self):
        '''Test the syntax highlighting of __declspec(dllimport)'''

        text = '''#define MACRO __declspec(dllimport)
MACRO void Function();'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsPreprocessor(highlighted_lines[0], '#define')
        self.assertHighlightedAsConstantName(highlighted_lines[0], 'MACRO')
        self.assertHighlightedAsConstantName(highlighted_lines[1], 'MACRO')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_declspec_dllexport(self):
        '''Test the syntax highlighting of __declspec(dllexport)'''

        text = '''__declspec(dllexport)'''
        highlighted_line = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_line, '__declspec')
        self.assertHighlightedAsPunctuation(highlighted_line, '(')
        self.assertHighlightedAsPreprocessor(highlighted_line, 'dllexport')
        self.assertHighlightedAsPunctuation(highlighted_line, ')')
        self.assertNothingWasStripped(text, highlighted_line)

    def test_declspec_dllimport(self):
        '''Test the syntax highlighting of __declspec(dllimport)'''

        text = '''__declspec(dllimport)'''
        highlighted_line = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_line, '__declspec')
        self.assertHighlightedAsPunctuation(highlighted_line, '(')
        self.assertHighlightedAsPreprocessor(highlighted_line, 'dllimport')
        self.assertHighlightedAsPunctuation(highlighted_line, ')')
        self.assertNothingWasStripped(text, highlighted_line)

    def test_pragma_comment(self):
        '''Test the syntax highlighting of a pragma comment.'''

        text = '''#pragma comment(linker, "/export:_slFunction")'''

        highlighted_line = self.highlight(text)

        self.assertHighlightedAsPreprocessor(highlighted_line, '#pragma')
        self.assertHighlightedAsPreprocessor(highlighted_line, 'comment')
        self.assertHighlightedAsString(highlighted_line, '"/export:_slFunction"')
        self.assertNothingWasStripped(text, highlighted_line)

if __name__ == '__main__':
    from pygments import highlight
    from pygments.lexers import CppLexer
    from pygments.formatters import HtmlFormatter
    print highlight('#define MACRO', CppLexer(), HtmlFormatter())
    import logging
    logging.basicConfig(level=logging.DEBUG)
    import unittest
    unittest.main()
