'''Unit tests for module pygclang.'''

# disable warning about too many public methods and disable warning about locally disabling that warning
#pylint: disable=I0011,R0904

# disable warning about local import and disable warning about locally disabling that warning
#pylint: disable=I0011,W0403

from pygclang_fixture import ClangLexerTestFixture

class TestCHighlight(ClangLexerTestFixture):
    '''Test case for ClangLexer highlighting of c code.'''

    def test_variable_declaration(self):
        '''Test the syntax highlighting of a variable declaration.'''

        text = 'int variable;'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_text, 'int')
        self.assertHighlightedAsVariableName(highlighted_text, 'variable')
        self.assertHighlightedAsPunctuation(highlighted_text, ';')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_global_integer_assignment(self):
        '''Test the syntax highlighting of an integer assignment.'''

        text = 'auto v = 5;'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_text, 'auto')
        self.assertHighlightedAsVariableName(highlighted_text, 'v')
        self.assertHighlightedAsPunctuation(highlighted_text, '=')
        self.assertHighlightedAsInteger(highlighted_text, '5')
        self.assertHighlightedAsPunctuation(highlighted_text, ';')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_global_float_assignment(self):
        '''Test the syntax highlighting of a float assignment.'''

        text = 'auto v = 5.0;'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_text, 'auto')
        self.assertHighlightedAsVariableName(highlighted_text, 'v')
        self.assertHighlightedAsPunctuation(highlighted_text, '=')
        self.assertHighlightedAsFloat(highlighted_text, '5.0')
        self.assertHighlightedAsPunctuation(highlighted_text, ';')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_function_declaration(self):
        '''Test the syntax highlighting of a function declaration.'''

        text = 'float function();'
        highlighted_text = self.highlight(text)

        self.assertHighlightedAsKeyword(highlighted_text, 'float')
        self.assertHighlightedAsFunctionName(highlighted_text, 'function')
        self.assertHighlightedAsPunctuation(highlighted_text, '();')
        self.assertNothingWasStripped(text, highlighted_text)

    def test_function_parameter_reference(self):

        text = '''void function(int p)
{
  p;
}'''
        highlighted_lines = self.highlight(text).split('\n')
        self.assertHighlightedAsVariableName(highlighted_lines[2], 'p')
        self.assertNothingWasStripped(text, highlighted_lines)

    def test_extern_c(self):
        '''Test the syntax highlighting of a extern "C".'''

        text = '''extern "C"
{
}'''
        highlighted_lines = self.highlight(text).split('\n')

        self.assertHighlightedAsKeyword(highlighted_lines[0], 'extern')
        self.assertHighlightedAsString(highlighted_lines[0], '"C"')
        self.assertNothingWasStripped(text, highlighted_lines)

if __name__ == '__main__':
    import unittest
    unittest.main()
